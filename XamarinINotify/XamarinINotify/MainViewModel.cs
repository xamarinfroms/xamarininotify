﻿using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinINotify
{
    public class MainViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public MainViewModel()
        {
            this.DoAdd = new Command(() => this.Value += 1);
        }

        public int Value
        {
            get { return this.value; }
            set
            {
                this.value = value;
                this.FireEvent(nameof(this.Value));
            }
        }
        private int value;

        public void FireEvent(string name)
        {
            this.PropertyChanged?
                   .DynamicInvoke(this, new PropertyChangedEventArgs(name));
        }


        public ICommand DoAdd { get; }




    }


}
